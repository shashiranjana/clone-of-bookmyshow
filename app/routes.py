from flask import render_template, flash, redirect, url_for, request
from flask import render_template
from app import app, db
from app.forms import LoginForm, RegistrationForm, MoviesForm
from flask_login import current_user, login_user, logout_user,login_required
from app.models import User  , MoviesDetails, Seats
from werkzeug.urls import url_parse
from app.forms import TicketForm
from app.email import send_ticket_email

@app.route('/')
@app.route('/index')
def index():
    # movies = MoviesDetails.query.all()
    unique_movies=db.session.query(MoviesDetails.wall_paper_image, MoviesDetails.movies_name).distinct().all()
    return render_template('index.html', title='Movie Tickets, Plays, Sports, Events & Cinemas ', unique_movies =unique_movies)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        if current_user.role == 'admin':
            return redirect(url_for('admin'))
        else:
            return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        if current_user.role == 'admin':
            return redirect(url_for('admin'))
        else:
            return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/movies_pages/<movies_name>', methods=['GET', 'POST'])
def movies_page(movies_name):
    return render_template('movies_page.html', movies_name=movies_name)

@app.route('/book_tickets/<movie_name>', methods=['GET', 'POST'])
def book_ticket(movie_name):
    movies_details=MoviesDetails.query.filter_by(movies_name = movie_name).all()
    # unique_date=db.session.query(MoviesDetails.start_date).distinct().all()
    return render_template('book_ticket.html',movies_details=movies_details)

@app.route('/select_seats/<movie_name>/<theaters_name>/<date>/<time>', methods=['GET', 'POST'])
@login_required
def select_seat(movie_name,theaters_name,date,time):
    if current_user.is_authenticated:
        form = TicketForm()
        seats_details= Seats.query.filter_by(theater_name_value=theaters_name,movies_name_value=movie_name, start_date=date,start_time=time).all()
        total_seats_booked=[]
        for seat_no in seats_details:

            if ',' not in str(seat_no.seat_id) :
                seat_booked_by_one_user=seat_no.seat_id
                total_seats_booked.append(seat_booked_by_one_user)
            else:
                seat_booked_by_one_user=seat_no.seat_id.split(',')
                for seat in seat_booked_by_one_user:
                    total_seats_booked.append(int(seat))

        if form.validate_on_submit():
            # user = User.query.filter_by(email=form.email.data).first()
            user = current_user
            if user:
                send_ticket_email(user, form.your_seat.data,theaters_name, movie_name, date, time)
                user_seats_data= Seats(movies_name_value=movie_name,seat_id=form.your_seat.data,theater_name_value=theaters_name,start_date=date,start_time=time)
                db.session.add(user_seats_data)
                db.session.commit()
            flash('Congrats! Your ticket has booked.Check mail for ticket')

            return redirect(url_for('index'))
    else:
        return redirect(url_for('login'))
    return render_template('select_seat.html',form=form,total_seats_booked=total_seats_booked)

@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)

@app.route('/admin', methods=['GET', 'POST'])
def admin():
    if current_user.is_authenticated:
        movies_form = MoviesForm()
        if movies_form.validate_on_submit():
            movies_details = MoviesDetails(theater_name=movies_form.theater_name.data,theater_id_value=movies_form.theater_id_value.data,movies_name=movies_form.movies_name.data,movies_id=movies_form.movies_id.data, start_date=movies_form.start_date.data, start_time=movies_form.start_time.data, wall_paper_image=movies_form.wall_paper_image.data)
            db.session.add(movies_details)
            db.session.commit()
            return redirect(url_for('admin'))

            
        return render_template('admin.html', title='Admin Details', movies_form=movies_form)
    else:
        return redirect(url_for('login'))
    return render_template('admin.html', title='Admin Details', movies_form=movies_form)


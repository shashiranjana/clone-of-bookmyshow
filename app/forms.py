from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField , IntegerField
from wtforms.validators import DataRequired
from app.models import User, MoviesDetails
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo

class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')

class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')

class MoviesForm(FlaskForm):
    theater_id_value=IntegerField('Theater Id')
    theater_name=StringField('Theater Name')
    movies_name = StringField('Movies Name')
    movies_id = IntegerField('Movies Id')
    start_date = StringField('Select Date')
    start_time = StringField('Select Time')
    wall_paper_image = StringField('Image File Name')
    submit = SubmitField('Submit')


class TicketForm(FlaskForm):
    your_seat = StringField('Your Seat', validators=[DataRequired()])
    submit = SubmitField('Send Ticket')

# class SearchText(FlaskForm):
#     search = StringField('search')





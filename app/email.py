from flask_mail import Message
from app import mail
from flask import render_template
from app import app

def send_email(subject, sender, recipients, text_body, html_body):
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = html_body
    mail.send(msg)

def send_ticket_email(user, your_seat,theaters_name,movie_name, date,time):
    send_email('[BookMyShOw]  Your Ticket',
               sender=app.config['ADMINS'][0],
               recipients=[user.email],
               text_body=render_template('email/email_format.txt',user=user , your_seat=your_seat,theaters_name = theaters_name,movie_name = movie_name, date = date, time=time),
               html_body=render_template('email/email_format.html',
                                         user=user,  your_seat=your_seat , theaters_name = theaters_name,movie_name = movie_name, date = date, time=time))
from app import db, login
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from datetime import datetime


class User(UserMixin,db.Model):
    id = db.Column(db.Integer, primary_key=True)
    role = db.Column(db.String(50))
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.username)

@login.user_loader
def load_user(id):
    return User.query.get(int(id))

class MoviesDetails(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    theater_name = db.Column(db.String(140))
    theater_id_value=db.Column(db.Integer)
    movies_id = db.Column(db.Integer)
    movies_name = db.Column(db.String(140))
    start_date =db.Column(db.String(20))
    start_time =db.Column(db.String(20))
    wall_paper_image= db.Column(db.String(32))

    def __repr__(self):
        return '<Movies_Details {}>'.format(self.movies_name)

class Seats(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    movies_name_value = db.Column(db.Integer, db.ForeignKey('movies_details.movies_name'))
    seat_id = db.Column(db.Integer)
    theater_name_value = db.Column(db.String, db.ForeignKey('movies_details.theater_name'))
    start_date = db.Column(db.String, db.ForeignKey('movies_details.start_date'))
    start_time = db.Column(db.String, db.ForeignKey('movies_details.start_time'))

    def __repr__(self):
        return '<Seats {}>'.format(self.seat_id)




